package com.example.abysterpub.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class RequestValidationException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8758869293477264302L;

	public RequestValidationException(String message) {
		super(message);
	}
}
