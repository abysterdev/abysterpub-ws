package com.example.abysterpub.exceptions;

import org.springframework.http.HttpStatus;

public class AbysterpubFunctionalException extends Exception {
	HttpStatus httpCode= HttpStatus.BAD_REQUEST;
	public HttpStatus getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(HttpStatus httpCode) {
		this.httpCode = httpCode;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public AbysterpubFunctionalException(String message ) {
	 super(message)	;
	}

	public AbysterpubFunctionalException(String message, HttpStatus httpCode ) {
		 super(message);
		 this.setHttpCode(httpCode);
		}
	
	public AbysterpubFunctionalException(Exception ex ) {
		 super(ex)	;
	}
	
}
