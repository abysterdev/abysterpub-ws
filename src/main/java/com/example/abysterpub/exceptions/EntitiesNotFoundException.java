package com.example.abysterpub.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntitiesNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 618675053150171098L;

	public EntitiesNotFoundException(String message) {
		super(message);
	}
}
