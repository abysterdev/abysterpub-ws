package com.example.abysterpub.exceptions;

public class ErrorMessage {

	private String message;

	public ErrorMessage(String message2) {
		this.message=message2;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
