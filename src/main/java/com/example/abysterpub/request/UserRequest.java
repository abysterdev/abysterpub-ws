package com.example.abysterpub.request;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@ApiModel(description="all details about user")
public class UserRequest {
	@NotNull
	@Email(message="Email invalide")
	@ApiModelProperty(required=true,position=0,notes="user email")
	String email;
	@NotNull
	@ApiModelProperty(required=true,position=1,notes="user password")
	String password;
	@NotNull
	@ApiModelProperty(required=true,position=2,notes="user Role")
	String role;
	@ApiModelProperty(required=true,position=3)
	private LocalDateTime createDate;
	@ApiModelProperty(required=true,position=4)
	private LocalDateTime updateDate;
	
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}
	
	 public void setUpdateDate(LocalDateTime updateDate) {
			this.updateDate = updateDate;
		}
		   
		   
		public LocalDateTime getUpdateDate() {
			return updateDate;
		}

		public LocalDateTime getCreateDate() {
			return createDate;
		}
		
	public void setRole(String role) {
		this.role = role;
	}
	
	public String getRole() {
		return role;
	}
	
	public UserRequest() {
		super();
	}
	
	public UserRequest(String email,String password,String role,LocalDateTime createDate, LocalDateTime updateDate) {
			this.email=email;
	        this.password=password;
	        this.role=role;
	        this.updateDate=updateDate; 
	        this.createDate=createDate;
	}

	 public String getEmail() {
	        return email;
	    }

	    

	    public String getPassword() {
	        return password;
	    }

	    public void setEmail(String email) {
	    	this.email = email;
	    }

	   

	    public void setPassword(String password) {
	    	this.password = password;
	    }

}
