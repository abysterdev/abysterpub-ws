package com.example.abysterpub.request;

public class DiffusionRequest {
private String support_diffusion; 
	
	public DiffusionRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getSupport_diffusion() {
		return support_diffusion;
	}

	public void setSupport_diffusion(String support_diffusion) {
		this.support_diffusion = support_diffusion;
	} 
	
	public DiffusionRequest(String support_diffusion) {
		this.support_diffusion=support_diffusion;
	}
}
