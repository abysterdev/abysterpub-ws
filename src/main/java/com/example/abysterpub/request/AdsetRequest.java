package com.example.abysterpub.request;

import java.time.LocalDateTime;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class AdsetRequest {
	private String OPTIMIZATION_GOAL;
	private String billing_event;
	private String targeting;
	private String nom_adset;
	private String bouton_action;
	private int bid_amount; 
	private int budget;
	private String statut_adset;
	private LocalDateTime start_time;
	private LocalDateTime end_time;
	


	public String getOPTIMIZATION_GOAL() {
		return OPTIMIZATION_GOAL;
	}


	public void setOPTIMIZATION_GOAL(String oPTIMIZATION_GOAL) {
		OPTIMIZATION_GOAL = oPTIMIZATION_GOAL;
	}


	public String getBilling_event() {
		return billing_event;
	}

	
	public String getStatut_adset() {
		return statut_adset;
	}


	public void setStatut_adset(String statut_adset) {
		this.statut_adset = statut_adset;
	}

	public void setBilling_event(String billing_event) {
		this.billing_event = billing_event;
	}


	public String getTargeting() {
		return targeting;
	}


	public void setTargeting(String targeting) {
		this.targeting = targeting;
	}


	public String getNom_adset() {
		return nom_adset;
	}


	public void setNom_adset(String nom_adset) {
		this.nom_adset = nom_adset;
	}



	public String getBouton_action() {
		return bouton_action;
	}


	public void setBouton_action(String bouton_action) {
		this.bouton_action = bouton_action;
	}


	public int getBid_amount() {
		return bid_amount;
	}


	public void setBid_amount(int bid_amount) {
		this.bid_amount = bid_amount;
	}


	public int getBudget() {
		return budget;
	}


	public void setBudget(int budget) {
		this.budget = budget;
	}


	public LocalDateTime getStart_time() {
		return start_time;
	}


	public void setStart_time(LocalDateTime start_time) {
		this.start_time = start_time;
	}


	public LocalDateTime getEnd_time() {
		return end_time;
	}


	public void setEnd_time(LocalDateTime end_time) {
		this.end_time = end_time;
	}


	
	
	
	public AdsetRequest() {
		// TODO Auto-generated constructor stub
	}
	public AdsetRequest(String nom_adset,String targeting,String bouton_action,String optimization_goal,int bid_amount,int budget,LocalDateTime start_date,LocalDateTime end_date) {
		this.bid_amount=bid_amount; 
		this.billing_event=billing_event; 
		this.bouton_action=bouton_action; 
		this.budget=budget; 
		this.end_time=end_time; 
		this.start_time=start_time;
		this.nom_adset=nom_adset; 
		this.OPTIMIZATION_GOAL=optimization_goal;
		this.targeting=targeting;
	}
	
}
