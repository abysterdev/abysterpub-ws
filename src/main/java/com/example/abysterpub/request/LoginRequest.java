package com.example.abysterpub.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description="all details about login")
public class LoginRequest {
	@NotNull
	@ApiModelProperty(notes="user email",required=true)
	String email;
	@NotNull
	@ApiModelProperty(notes="user password",required=true)
	String password;
	public LoginRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoginRequest(@NotNull String email, @NotNull String password) {
		super();
		this.email = email;
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
