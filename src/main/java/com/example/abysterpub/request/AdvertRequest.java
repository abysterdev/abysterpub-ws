package com.example.abysterpub.request;

public class AdvertRequest {
	  private String accroche; 
	  private String titre; 
	  private String description; 
	  private String statut_pub; 
	  private String nom_publicite;
	
	public String getAccroche() {
		return accroche;
	}
	public void setAccroche(String accroche) {
		this.accroche = accroche;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatut_pub() {
		return statut_pub;
	}
	public void setStatut_pub(String statut_pub) {
		this.statut_pub = statut_pub;
	}
	public String getNom_publicite() {
		return nom_publicite;
	}
	public void setNom_publicite(String nom_publicite) {
		this.nom_publicite = nom_publicite;
	} 
	  
	  public AdvertRequest() {
		// TODO Auto-generated constructor stub
	}
	  
	  
}
