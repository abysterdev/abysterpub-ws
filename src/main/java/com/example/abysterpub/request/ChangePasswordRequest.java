package com.example.abysterpub.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Modification de mot de passe")
public class ChangePasswordRequest {
	@ApiModelProperty(allowEmptyValue = false , required = true,value = " OTP Code reçu au préalable" )
	private String otp;

	@ApiModelProperty(allowEmptyValue = false , required = true,value = " Nouveau mot de passe" )
    private String newPassword ;
    
	@ApiModelProperty(allowEmptyValue = false, required = true,value = " Email du compte" )
    private String email;

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public ChangePasswordRequest() {
		
	}
	
	public ChangePasswordRequest(String email,String newPassword, String otp) {
		this.email=email; 
		this.newPassword=newPassword; 
		this.otp=otp;
	}
}
