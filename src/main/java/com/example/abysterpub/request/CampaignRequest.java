package com.example.abysterpub.request;

public class CampaignRequest {

	private String nom;
	private String objectif; 
	private String special_ad_categorie; 
	private String statut_campagne; 
	
	public void setSpecial_ad_categorie(String special_ad_categorie) {
		this.special_ad_categorie = special_ad_categorie;
	}
	public String getNom() {
		return nom;
	}
	public String getObjectif() {
		return objectif;
	}
	public String getSpecial_ad_categorie() {
		return special_ad_categorie;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getStatut_campagne() {
		return statut_campagne;
	}
	public void setObjectif(String objectif) {
		this.objectif = objectif;
	}
	public void setStatut_campagne(String statut_campagne) {
		this.statut_campagne = statut_campagne;
	}
	
	
	
	
	public CampaignRequest() {
		
	}

	public CampaignRequest(String nom, String objectif,String special_ad_categorie,String statut_campagne) {
		this.nom=nom; 
		this.objectif=objectif; 
		this.special_ad_categorie=special_ad_categorie; 
		this.statut_campagne=statut_campagne;
	}
}
