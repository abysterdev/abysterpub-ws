package com.example.abysterpub.request;

public class PaymentMomoRequest {
	 private String nom_operateur;
	    private String cle_marchand; 
	    
	    public void setNom_operateur(String nom_operateur) {
			this.nom_operateur = nom_operateur;
		}
		public String getCle_marchand() {
			return cle_marchand;
		}
		public void setCle_marchand(String cle_marchand) {
			this.cle_marchand = cle_marchand;
		}
		
		public PaymentMomoRequest() {
		
		}
		public String getNom_operateur() {
			return nom_operateur;
		}
		
		public PaymentMomoRequest(String nom_operateur,String cle_marchand) {
			this.cle_marchand=cle_marchand; 
			this.nom_operateur=nom_operateur;
		}
}
