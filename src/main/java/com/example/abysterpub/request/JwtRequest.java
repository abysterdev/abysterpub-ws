package com.example.abysterpub.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 *
 * This class is required for storing the username and password we receive from the client.
 */
@ApiModel(description="all details about json web token parameters")
public class JwtRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	@NotNull
	@ApiModelProperty(notes="user email",required=true)
	private String username;//username of user
	@NotNull
	@ApiModelProperty(notes="user password",required=true)
	private String password;//password of user
	@NotNull
	@ApiModelProperty(notes="application granted token",required=true)
	private String application; //token of granted application
	
	public JwtRequest() {
		super();
	}
	public JwtRequest(String username, String password,String application) {
		super();
		this.username = username;
		this.password = password;
		this.application =  application;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	
	
	
}
