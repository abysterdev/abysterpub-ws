package com.example.abysterpub.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.abysterpub.entities.adset;

@Repository
public interface adsetRepository extends CrudRepository<adset, Integer> {
	
}
