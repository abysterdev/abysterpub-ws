package com.example.abysterpub.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.abysterpub.entities.Diffusion;

public  interface DiffusionRepository extends CrudRepository<Diffusion,Integer> {
	
}
