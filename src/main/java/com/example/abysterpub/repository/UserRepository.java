package com.example.abysterpub.repository;

import com.example.abysterpub.entities.Utilisateur;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<Utilisateur, Integer>{
  //  public List<User> findAllUsers();
  // public Utilisateur findUserById(int id);
	public Utilisateur findByEmailAndPassword(String email,String password);
	Utilisateur findByToken(String token);
	Utilisateur findByEmail(String email);
	//public List<Utilisateur> findByIdOrEmail(int Id,String email);
	
	
}