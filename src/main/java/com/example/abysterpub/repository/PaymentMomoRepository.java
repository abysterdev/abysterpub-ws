package com.example.abysterpub.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.abysterpub.entities.Paiement_mobile_money;

public  interface PaymentMomoRepository extends CrudRepository<Paiement_mobile_money,Integer> {

}
