package com.example.abysterpub.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.abysterpub.entities.Publicite;

public  interface AdvertRepository extends CrudRepository<Publicite, Integer> {

}
