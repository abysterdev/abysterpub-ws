package com.example.abysterpub.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.abysterpub.entities.Campagne;

public  interface CampaignRepository extends CrudRepository<Campagne,Integer> {

}
