package com.example.abysterpub.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.abysterpub.entities.Application;

@Repository
public interface ApplicationRepository extends CrudRepository<Application, Integer> {

	public Application findByToken(String token);
	public Application findByName(String name);
}
