package com.example.abysterpub.security;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.abysterpub.entities.Utilisateur;
import com.example.abysterpub.repository.UserRepository;
/**

 *
 * JWTUserDetailsService implements the Spring Security UserDetailsService interface. 
 * It overrides the loadUserByUsername for fetching userRequest details from the database using 
 * the username. The Spring Security Authentication Manager calls this method for 
 * getting the userRequest details from the database when authenticating the userRequest details provided by the userRequest.
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {
Logger logger=LoggerFactory.getLogger(JwtUserDetailsService.class);
	
	@Autowired
	UserRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Utilisateur user= repository.findByEmail(username);
		if (user==null) {
			logger.error("User not found with username: " + username);
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),
					new ArrayList<>());
	}

}
