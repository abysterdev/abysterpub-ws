package com.example.abysterpub.service;

import com.example.abysterpub.entities.Utilisateur;
import com.example.abysterpub.exceptions.AbysterpubFunctionalException;
import com.example.abysterpub.request.ChangePasswordRequest;
import com.example.abysterpub.request.OtpRequest;
import com.example.abysterpub.request.tokenRequest;

public interface IUserService {
	public Utilisateur requestOtpPassword(String email) throws AbysterpubFunctionalException;
	public Utilisateur changePwd(ChangePasswordRequest changePwdIn)throws AbysterpubFunctionalException,Exception;
}
