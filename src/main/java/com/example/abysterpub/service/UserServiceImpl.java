package com.example.abysterpub.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.ConstraintViolationException;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.example.abysterpub.entities.Utilisateur;
import com.example.abysterpub.exceptions.AbysterpubFunctionalException;
import com.example.abysterpub.repository.UserRepository;
import com.example.abysterpub.request.ChangePasswordRequest;
import com.example.abysterpub.request.OtpRequest;
import com.example.abysterpub.request.tokenRequest;
import com.example.abysterpub.utils.Otp;
import com.example.abysterpub.utils.OtpImpl;
import com.sun.jersey.api.client.ClientResponse;

@Service
public class UserServiceImpl implements IUserService{
	
	//private static final long EXPIRE_TOKEN_AFTER_MINUTES = 30;
	@Autowired
	private UserRepository userRepository;
	
	@Autowired 
	Otp otp;
    @Autowired
    PasswordEncoder passwordEncoder;
	/*
	public String forgotPassword(String email) {

		Optional<Utilisateur> userOptional = Optional
				.ofNullable(userRepository.findByEmail(email));

		if (!userOptional.isPresent()) {
			return "Invalid email id.";
		}

		Utilisateur user = userOptional.get();
		user.setToken(generateToken());
		user.setCreateDate(LocalDateTime.now());

		user = userRepository.save(user);

		return user.getToken();
	}

	public String resetPassword(String token, String password) {

		Optional<Utilisateur> userOptional = Optional
				.ofNullable(userRepository.findByToken(token));

		if (!userOptional.isPresent()) {
			return "Invalid token.";
		}

		LocalDateTime tokenCreationDate = userOptional.get().getCreateDate();

		if (isTokenExpired(tokenCreationDate)) {
			return "Token expired.";

		}

		Utilisateur user = userOptional.get();

		user.setPassword(password);
		user.setToken(null);
		user.setCreateDate(null);

		userRepository.save(user);

		return "Your password successfully updated.";
	}

	
	private String generateToken() {
		StringBuilder token = new StringBuilder();

		return token.append(UUID.randomUUID().toString())
				.append(UUID.randomUUID().toString()).toString();
	}

	
	private boolean isTokenExpired(final LocalDateTime tokenCreationDate) {

		LocalDateTime now = LocalDateTime.now();
		Duration diff = Duration.between(tokenCreationDate, now);

		return diff.toMinutes() >= EXPIRE_TOKEN_AFTER_MINUTES;
	}
	*/
	private void generateAndSendOTPByEmail(Utilisateur utilisateur) {

		String token =String.valueOf((int)(Math.random()*((999999-100000)+1))+100000);
		utilisateur.setToken(token);
		utilisateur.setUpdateDate(utilisateur.getUpdateDate());
		//logger.info(String.format("Token %s generated for account %s", token,account.getAccountId()));
		userRepository.save(utilisateur);
		String email=utilisateur.getEmail();
		otp.sendActivationCodeByEmail(email,token);

	}
	
	@Override
	public Utilisateur requestOtpPassword(String email) throws AbysterpubFunctionalException{
		try {
		
		Utilisateur existingUsers = userRepository.findByEmail(email);
		
		
		if (existingUsers==null) {
			throw new AbysterpubFunctionalException(String.format("Account with email [%s] not found", email));
		}
		// Validate mandatory parameters
		tokenRequest tokreq=new tokenRequest();
		//Utilisateur utilisateur=existingUsers.get();
		generateAndSendOTPByEmail(existingUsers);
		existingUsers.setUpdateDate(LocalDateTime.now());
		return existingUsers;
	} catch( ConstraintViolationException cve) {
		//logger.error(cve.getMessage());
		throw new AbysterpubFunctionalException(cve);
	} catch (HibernateException he) {
		//logger.error(he.getLocalizedMessage());
		throw new AbysterpubFunctionalException(he);
	} catch (Exception e) {
		//logger.warn(e.getLocalizedMessage());
		throw e;
	}
	
		
	
	}
/*
	@Override
	public tokenRequest requestOtpPassword(OtpRequest otp) throws AbysterpubFunctionalException{
		try {
		String email=otp.getEmail(); 
		int id=otp.getAccountId();
		List<Utilisateur> existingAccounts = userRepository.findByAccountIdOrEmail(id,email);
		if (StringUtils.isEmpty(email) && id==0) {
			throw new AbysterpubFunctionalException("requestOtpPassword: Email or AccountId is mandatory for this operation. ");
		}
		
		if (existingAccounts.isEmpty()) {
			throw new AbysterpubFunctionalException(String.format("Account with email [%s] not found", otp.getEmail()));
		}
		// Validate mandatory parameters
		tokenRequest tokreq=new tokenRequest();
		Utilisateur utilisateur=existingAccounts.get(0);
		generateAndSendOTPByEmail(utilisateur);
		tokreq.setToken(utilisateur.getToken());
		
	} catch( ConstraintViolationException cve) {
		//logger.error(cve.getMessage());
		throw new AbysterpubFunctionalException(cve);
	} catch (HibernateException he) {
		//logger.error(he.getLocalizedMessage());
		throw new AbysterpubFunctionalException(he);
	} catch (Exception e) {
		//logger.warn(e.getLocalizedMessage());
		throw e;
	}
	return null;
		
	
	}
*/

	@Override
	public Utilisateur changePwd(ChangePasswordRequest changePwdIn) throws AbysterpubFunctionalException, Exception {
		try {

			// Validate mandatory parameters
			String email = changePwdIn.getEmail();
			if (StringUtils.isEmpty(email )) {
				throw new AbysterpubFunctionalException("Email is mandatory for password updates. ");
			}
			String otp = changePwdIn.getOtp();
			if (StringUtils.isEmpty(otp)) {
				throw new AbysterpubFunctionalException(" OTP Password is required");
			}
			
			email=email.trim().toLowerCase();
			Utilisateur existingAccounts = userRepository.findByEmail(email);
			if (ObjectUtils.isEmpty(existingAccounts)) {
				throw new AbysterpubFunctionalException(String.format("Invalid account with details [%s] ",email));
			}
			//invalid OTP cases
		//Utilisateur util = existingAccounts.get() ;
			if(! otp.equalsIgnoreCase( existingAccounts.getToken())) {
				//logger.info("OTP received %s OTP known %s",otp, account.getToken());
				throw new AbysterpubFunctionalException(String.format("Invalid OTP [%s] ",otp));
			}
			//so far so good
			existingAccounts.setPassword(passwordEncoder.encode(changePwdIn.getNewPassword()));
			 //util.setActive(1);
			existingAccounts.setUpdateDate(LocalDateTime.now());
		
			//out = new AccountOut();
			//out.setEmail(account.getEmail());
			//out.setStatus(account.getActive());
			//out.setUpdateDate(account.getUpdateDate());
			return userRepository.save(existingAccounts);
		} catch( ConstraintViolationException cve) {
			//logger.error(cve.getMessage());
			throw new AbysterpubFunctionalException(cve);
		} catch (HibernateException he) {
			//logger.error(he.getLocalizedMessage());
			throw new AbysterpubFunctionalException(he);
		} catch (Exception e) {
			//logger.error(e.getLocalizedMessage());
			throw e;
		}
		
		
	}
}
