package com.example.abysterpub.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="application")
public class Application {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int applicationId;
	String name;
	String token;
	boolean active=true;
	Date creation;
	/**
	 * constructors
	 */
	
	public Application() {
		super();
	}
	public Application(int applicationId, String name, String token, boolean active,Date creation) {
		super();
		this.applicationId = applicationId;
		this.name = name;
		this.token = token;
		this.active = active;
		this.creation = creation;
	}
	
	/**
	 * getters and setters
	 * 
	 */
	
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getCreation() {
		return creation;
	}
	public void setCreation(Date creation) {
		this.creation = creation;
	}
	
}
