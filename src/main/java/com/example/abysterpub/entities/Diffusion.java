package com.example.abysterpub.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="diffusion")
public class Diffusion {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id_diffusion;
	@ManyToOne
	@JoinColumn(name="id_utilisateur") 
	Utilisateur utilisateur;
	private String support_diffusion; 
	
	public Diffusion() {
		// TODO Auto-generated constructor stub
	}

	public String getSupport_diffusion() {
		return support_diffusion;
	}

	public void setSupport_diffusion(String support_diffusion) {
		this.support_diffusion = support_diffusion;
	} 
	
	public Diffusion(String support_diffusion) {
		this.support_diffusion=support_diffusion;
	}
}
