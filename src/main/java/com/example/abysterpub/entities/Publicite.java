package com.example.abysterpub.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="publicite")
public class Publicite {
	  @Id
	    @GeneratedValue(strategy=GenerationType.IDENTITY)
	  private int id_publicite;
	  @Column(name="message")
	  private String message; 
	  @Column(name="titre")
	  private String titre; 
	  @Column(name="description")
	  private String description; 
	  @Column(name="statut")
	  private String statut; 
	  @Column(name="nom")
	  private String nom;
	  
	  @ManyToOne
	  @JsonIgnore
		@JoinColumn(name="id_utilisateur") 
		private Utilisateur utilisateur;
	  
	  
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	  
	
	
	  public int getId_publicite() {
		return id_publicite;
	}
	public void setId_publicite(int id_publicite) {
		this.id_publicite = id_publicite;
	}
	public Publicite() {
		// TODO Auto-generated constructor stub
	}
	  
	
}
