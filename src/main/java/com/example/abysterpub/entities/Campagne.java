package com.example.abysterpub.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="campagne")
public class Campagne {
	

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id_campagne;
	private String nom;
	private String objectif; 
	private String special_ad_categorie; 
	private String statut_campagne; 
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="id_utilisateur") 
	private Utilisateur utilisateur;
	
	public void setSpecial_ad_categorie(String special_ad_categorie) {
		this.special_ad_categorie = special_ad_categorie;
	}
	public String getNom() {
		return nom;
	}
	public String getObjectif() {
		return objectif;
	}
	public String getSpecial_ad_categorie() {
		return special_ad_categorie;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getStatut_campagne() {
		return statut_campagne;
	}
	public void setObjectif(String objectif) {
		this.objectif = objectif;
	}
	public void setStatut_campagne(String statut_campagne) {
		this.statut_campagne = statut_campagne;
	}
	
	public void setId_campagne(int id_campagne) {
		this.id_campagne = id_campagne;
	}
	public int getId_campagne() {
		return id_campagne;
	}
	
	public Campagne() {
		
	}

	public Campagne(String nom, String objectif,String special_ad_categorie,String statut_campagne) {
		this.nom=nom; 
		this.objectif=objectif; 
		this.special_ad_categorie=special_ad_categorie; 
		this.statut_campagne=statut_campagne;
	}
}
