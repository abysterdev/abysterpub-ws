package com.example.abysterpub.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="adset")

public class adset {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int adset_id;
	@Column(name="OPTIMIZATION_GOAL")
	private String OPTIMIZATION_GOAL;
	@Column(name="billing_event")
	private String billing_event;
	@Column(name="targeting")
	private String targeting;
	@Column(name="nom")
	private String nom;
	@Column(name="statut")
	private String statut;
	@Column(name="cto")
	private String cto;
	@Column(name="bid_amount")
	private int bid_amount; 
	@Column(name="budget")
	private int budget;
	@Column(name="type_budget")
	private String type_budget;
	@Column(name="start_time")
	private LocalDateTime start_time;
	@Column(name="end_time")
	private LocalDateTime end_time;
	@ManyToOne
	@JsonIgnore
	 @JoinColumn(name="id_utilisateur")
	 private Utilisateur user;
	
	
	public int getAdset_id() {
		return adset_id;
	}


	public void setAdset_id(int adset_id) {
		this.adset_id = adset_id;
	}


	public String getOPTIMIZATION_GOAL() {
		return OPTIMIZATION_GOAL;
	}


	public void setOPTIMIZATION_GOAL(String oPTIMIZATION_GOAL) {
		OPTIMIZATION_GOAL = oPTIMIZATION_GOAL;
	}


	public String getBilling_event() {
		return billing_event;
	}


	public void setBilling_event(String billing_event) {
		this.billing_event = billing_event;
	}


	public String getTargeting() {
		return targeting;
	}


	public void setTargeting(String targeting) {
		this.targeting = targeting;
	}



	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getStatut() {
		return statut;
	}


	public void setStatut(String statut) {
		this.statut = statut;
	}


	public String getCto() {
		return cto;
	}


	public void setCto(String cto) {
		this.cto = cto;
	}


	public String getType_budget() {
		return type_budget;
	}


	public void setType_budget(String type_budget) {
		this.type_budget = type_budget;
	}


	public Utilisateur getUser() {
		return user;
	}


	public void setUser(Utilisateur user) {
		this.user = user;
	}


	public int getBid_amount() {
		return bid_amount;
	}


	public void setBid_amount(int bid_amount) {
		this.bid_amount = bid_amount;
	}


	public int getBudget() {
		return budget;
	}


	public void setBudget(int budget) {
		this.budget = budget;
	}


	public LocalDateTime getStart_time() {
		return start_time;
	}


	public void setStart_time(LocalDateTime start_time) {
		this.start_time = start_time;
	}


	public LocalDateTime getEnd_time() {
		return end_time;
	}


	public void setEnd_time(LocalDateTime end_time) {
		this.end_time = end_time;
	}


	
	
	
	public adset() {
		// TODO Auto-generated constructor stub
	}

	
}
