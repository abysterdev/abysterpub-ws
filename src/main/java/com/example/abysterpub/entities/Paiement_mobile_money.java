package com.example.abysterpub.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="paiement_mobile_money")
public class Paiement_mobile_money {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id_paiement;
    private String nom_operateur;
    private String cle_marchand; 
    @ManyToOne
	@JoinColumn(name="id_utilisateur") 
	Utilisateur utilisateur;
	public String getNom_operateur() {
		return nom_operateur;
	}
	public void setNom_operateur(String nom_operateur) {
		this.nom_operateur = nom_operateur;
	}
	public String getCle_marchand() {
		return cle_marchand;
	}
	public void setCle_marchand(String cle_marchand) {
		this.cle_marchand = cle_marchand;
	}
	
	public Paiement_mobile_money() {
	
	}
	
	
	public Paiement_mobile_money(String nom_operateur,String cle_marchand) {
		this.cle_marchand=cle_marchand; 
		this.nom_operateur=nom_operateur;
	}
}
