package com.example.abysterpub.entities;


import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;



import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;


@Entity
@Table(name="utilisateur")
public class Utilisateur {

	@ApiModelProperty(required=true,position=0)
    @Id  
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id_utilisateur;
    @NotEmpty 
    @Email
    @ApiModelProperty(required=true,position=1)
    @Column(unique = true,name="email")
   private String email; 
   @NotEmpty  
   @JsonIgnore
   @ApiModelProperty(required=true,position=2)
   @Column(name="password")
   private String password;
   @Column(name="role")
   @ApiModelProperty(required=true,position=3)
   private String role;
   //@ApiModelProperty(required=true,position=4)
   /*
   @JsonIgnore
   private boolean actif;
   */
   @Column(columnDefinition = "TIMESTAMP")
   @ApiModelProperty(required=true,position=4)
   private LocalDateTime createDate;
   @ApiModelProperty(required=true,position=5)
   private LocalDateTime updateDate;
   @JsonIgnore
   private String token;
   @OneToMany(cascade=CascadeType.ALL,mappedBy="utilisateur")
   private List<Campagne> campagne;
   @OneToMany(cascade=CascadeType.ALL,mappedBy="user")
   private List<adset> adset;
   @OneToMany(cascade=CascadeType.ALL,mappedBy="utilisateur")
   private List<Publicite> pub;
   
   public List<adset> getAdset() {
	return adset;
}

public void setAdset(List<adset> adset) {
	this.adset = adset;
}

public List<Publicite> getPub() {
	return pub;
}

public void setPub(List<Publicite> pub) {
	this.pub = pub;
}

public List<Campagne> getCampagne() {
	return campagne;
}

public void setCampagne(List<Campagne> campagne) {
	this.campagne = campagne;
}

public void setToken(String token) {
	this.token = token;
}
   
   public String getToken() {
	return token;
}
   
   public void setCreateDate(LocalDateTime localDateTime) {
	this.createDate = localDateTime;
}
   public void setUpdateDate(LocalDateTime updateDate) {
	this.updateDate = updateDate;
}
   
   
public int getId_utilisateur() {
	return id_utilisateur;
}

public void setId_utilisateur(int id_utilisateur) {
	this.id_utilisateur = id_utilisateur;
}

public LocalDateTime getUpdateDate() {
	return updateDate;
}

public  LocalDateTime getCreateDate() {
	return createDate;
}

/*
   public void setActif(boolean actif) {
	this.actif = actif;
}
   
   
   public boolean IsActif() {
	return actif;
}
*/

   public void setRole(String role) {
       this.role = role;
   }
   public String getRole() {
       return role;
   }
   public int getId() {
       return id_utilisateur;
   }
    public Utilisateur() {

    }


    public Utilisateur(String Email, String Password,String Role, LocalDateTime createDate, LocalDateTime updateDate,String token){
        this.email=Email;
        this.password=Password;
        this.role=Role;
        this.updateDate=updateDate; 
        this.createDate=createDate;
        this.token=token;
    }

  
    public String getEmail() {
        return email;
    }

    

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
    	this.email = email;
    }

   

    public void setPassword(String password) {
    	this.password = password;
    }

  

   
}