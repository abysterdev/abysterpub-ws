package com.example.abysterpub.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import javax.ws.rs.core.MediaType;

@Service
public class OtpImpl implements Otp{
	 public static  String TEMPLATE_ACTIVATION_MAIL="optivac-account-activate";
	    public static  String MAILGUN_FROM="from";
	    public static  String MAILGUN_TO="to";
	    public static  String MAILGUN_BCC="bcc";
	    public static  String MAILGUN_SUBJECT="subject";
	    public static  String MAILGUN_VARIABLES="h:X-Mailgun-Variables";
	    public static  String MAILGUN_TEMPLATE="template";
	    public static  String MAILGUN_RECIPIENTS="recipient-variables";
	    private static final String SBJ_CODE_TEMPORAIRE = "Votre code temporaire";
	    
	@Value("${mailgun.api.url}")
    private String  mailgunUrl;
    
    @Value("${mailgun.api.key}")
    private String mailgunApiKey;
    
    @Value("${mailgun.api.sender.name}")
    private String mailgunSenderName;
    
    @Value("${mailgun.api.sender.email}")
    private String mailgunSenderEmail;
    @Override
	 public  ClientResponse sendActivationCodeByEmail(String reciveEmail, String activationCode) {
	    	
	        Client client = Client.create();
	        client.addFilter(new HTTPBasicAuthFilter("api", mailgunApiKey));
	        WebResource webResource = client.resource(mailgunUrl);
	        MultivaluedMapImpl formData = new MultivaluedMapImpl();
	        formData.add(MAILGUN_FROM, mailgunSenderName+"<"+mailgunSenderEmail+">");
	        formData.add(MAILGUN_TO,"<"+reciveEmail+">");
			formData.add(MAILGUN_SUBJECT,SBJ_CODE_TEMPORAIRE );
	        formData.add(MAILGUN_TEMPLATE, TEMPLATE_ACTIVATION_MAIL);
	        formData.add(MAILGUN_VARIABLES, "{\"code\": \""+activationCode+"\"}");
	        //logger.info(String.format("email Request: %s",formData));
	        return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
	    }

	

}
