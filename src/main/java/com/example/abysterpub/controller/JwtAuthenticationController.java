package com.example.abysterpub.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.abysterpub.entities.Application;
import com.example.abysterpub.exceptions.AbysterpubFunctionalException;
import com.example.abysterpub.repository.ApplicationRepository;
import com.example.abysterpub.request.JwtRequest;
import com.example.abysterpub.response.JwtResponse;
import com.example.abysterpub.security.JwtUserDetailsService;
import com.example.abysterpub.utils.JwtTokenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(description="use  username, password and application token. If the credentials are valid, a JWT token is created")
public class JwtAuthenticationController {
	Logger logger=LoggerFactory.getLogger(JwtAuthenticationController.class);
	
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	ApplicationRepository appRepository;

	@ApiOperation(value="validate credentials",response=JwtResponse.class)
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	 @ApiImplicitParam(name="Access-Control-Allow-Origin", value="*",paramType="header",required=true)
	public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody JwtRequest authenticationRequest) throws Exception {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());
		
		//verify if the calling app is granted
		Application app=appRepository.findByToken(authenticationRequest.getApplication());
		if(app==null || (app!=null && !app.isActive())) {
			logger.info("l'Application n'est pas autorisée a accéder à l'API!");
			throw new AbysterpubFunctionalException("l'Application n'est pas autorisée a accéder à l'API!");
		}
			
		final String token = jwtTokenUtil.generateToken(userDetails,app.getName());

		return ResponseEntity.ok(new JwtResponse(token));
	}

	private void authenticate(String name, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(name, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
