package com.example.abysterpub.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.abysterpub.entities.Publicite;
import com.example.abysterpub.exceptions.EntitiesNotFoundException;
import com.example.abysterpub.repository.AdvertRepository;
import com.example.abysterpub.request.AdvertRequest;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

@RestController
@RequestMapping("/advert")
public class AdvertController {

	@Autowired 
	AdvertRepository advertrepository;
	
	@ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
	@PostMapping(value = "/create")
	public Publicite createAdvert(@Valid @RequestBody AdvertRequest advertrequest) {
		Publicite publicite=new Publicite(); 
		publicite.setId_publicite(publicite.getId_publicite());
		publicite.setMessage(advertrequest.getAccroche());
		publicite.setDescription(advertrequest.getDescription());
		publicite.setNom(advertrequest.getNom_publicite());
		publicite.setStatut(advertrequest.getStatut_pub());
		publicite.setTitre(advertrequest.getTitre());
		return advertrepository.save(publicite);
	}
	/*
	@DeleteMapping(value="/deleteById")
	public void findById(@RequestParam int id) {
		advertrepository.deleteById(id);
	}
	*/
	/*
	@PutMapping(value="/update/{id}")
	public Publicite updateAdvert(@Valid @RequestBody AdvertRequest advertrequest,@PathVariable int id) {
		Publicite publicite= advertrepository.findById(id).orElse(null);
		if(publicite!=null) {
			publicite.setAccroche(advertrequest.getAccroche());
			publicite.setDescription(advertrequest.getDescription());
			publicite.setFormat(advertrequest.getFormat());
			publicite.setNom_publicite(advertrequest.getNom_publicite());
			publicite.setStatut_pub(advertrequest.getStatut_pub());
			publicite.setTitre(advertrequest.getTitre());
			return advertrepository.save(publicite);	
		}
		throw new EntitiesNotFoundException("le type de compte avec l'identifiant "+id+" est introuvable.");
		
	}
	*/
}
