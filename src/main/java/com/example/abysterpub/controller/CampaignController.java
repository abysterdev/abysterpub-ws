package com.example.abysterpub.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.abysterpub.entities.Campagne;
import com.example.abysterpub.repository.CampaignRepository;
import com.example.abysterpub.request.CampaignRequest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

@RestController
@RequestMapping("/campaign")
public class CampaignController {

	
	 @Autowired 
	 CampaignRepository campaignrepository;
	 
	 @ApiImplicitParams({
	        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
	        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
	        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
		})
	 @PostMapping(value = "/create")
	 public Campagne createCampaign(@Valid @RequestBody CampaignRequest campaignrequest) {
		 Campagne campagne= new Campagne();
		 campagne.setId_campagne(campagne.getId_campagne());
		 campagne.setNom(campaignrequest.getNom());
		 campagne.setObjectif(campaignrequest.getObjectif());
		 campagne.setSpecial_ad_categorie(campaignrequest.getSpecial_ad_categorie());
		 campagne.setStatut_campagne(campaignrequest.getStatut_campagne());
		 Campagne addcampaign= campaignrepository.save(campagne);
				return addcampaign;
		 
	 }
	
	 @GetMapping(value = "/findById")
	 public void findById(@RequestParam int id) {
		 campaignrepository.findById(id);
	 }
	 
	 
	 @DeleteMapping(value="/deleteCampaignById") 
	 public void deleteCampaignById(@RequestParam int id) {
		 campaignrepository.deleteById(id);
	 }
	
	 
	 @ApiImplicitParams({
	        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
	        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
	        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
		})
	 @PutMapping(value="/update_campaign")
	 public Campagne updateCampaign(@Valid @RequestBody CampaignRequest campaignrequest) {
		 
		 Campagne campagne= new Campagne();
		 campagne.setId_campagne(campagne.getId_campagne());
		 campagne.setNom(campaignrequest.getNom());
		 campagne.setObjectif(campaignrequest.getObjectif());
		 campagne.setSpecial_ad_categorie(campaignrequest.getSpecial_ad_categorie());
		 campagne.setStatut_campagne(campaignrequest.getStatut_campagne());
		 Campagne addcampaign= campaignrepository.save(campagne);
				return addcampaign;
	 }
	 
	 
}
