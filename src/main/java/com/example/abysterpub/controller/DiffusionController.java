package com.example.abysterpub.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.abysterpub.entities.Diffusion;
import com.example.abysterpub.repository.DiffusionRepository;
import com.example.abysterpub.request.DiffusionRequest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

@RestController
@RequestMapping("/diffusion")
public class DiffusionController {
	@Autowired 
	DiffusionRepository diffusionrep;
	
	@ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
	 @PostMapping(value = "/add")
	 public Diffusion addDiffusion(@Valid @RequestBody DiffusionRequest diffuionreq) {
		 Diffusion diffusion=new Diffusion(); 
		 diffusion.setSupport_diffusion(diffuionreq.getSupport_diffusion());
		 return diffusionrep.save(diffusion);
	 }
}
