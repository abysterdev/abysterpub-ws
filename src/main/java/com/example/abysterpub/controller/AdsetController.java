package com.example.abysterpub.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.abysterpub.entities.Utilisateur;
import com.example.abysterpub.entities.adset;
import com.example.abysterpub.repository.adsetRepository;
import com.example.abysterpub.request.AdsetRequest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/adset")
public class AdsetController {

	@Autowired 
	adsetRepository adRep;
	
	@ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
	
    @ApiOperation(value="Add an Adset")
    @PostMapping(value = "/add")
	
	public adset addAdset(@Valid @RequestBody AdsetRequest adsetrequest) {
		adset adset= new adset();
		adset.setAdset_id(adset.getAdset_id());
		adset.setNom(adsetrequest.getNom_adset());
		adset.setBid_amount(adsetrequest.getBid_amount());
		adset.setBilling_event(adsetrequest.getBilling_event());
		adset.setCto(adsetrequest.getBouton_action());
		adset.setBudget(adsetrequest.getBudget());
		adset.setEnd_time(adsetrequest.getEnd_time());
		adset.setStart_time(adsetrequest.getStart_time());
		adset.setStatut(adsetrequest.getStatut_adset());
		adset.setOPTIMIZATION_GOAL(adsetrequest.getOPTIMIZATION_GOAL());
		adset added= adRep.save(adset);
		return added;	
	}
	
	@ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
    @GetMapping(value = "/findById")
    @ApiOperation(value="find an adset by his Id")
    public Optional<adset> findAdsetById (@RequestParam int id) {
		return adRep.findById(id);
    }
    
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})

    @ApiOperation(value="delete an Adset")
    @DeleteMapping(value = "/delete")
    public void deleteAdsetById (@RequestParam int id) {
		 adRep.deleteById(id);
    }
    
    
    
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
    @ApiOperation(value="find all Adsets from an account")
    @GetMapping(value = "/findAll")
    
    public void findAllAdsets(@RequestParam Iterable<Integer> id) {
    	adRep.findAllById(id);
    }
    
    
}
