package com.example.abysterpub.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.abysterpub.entities.Paiement_mobile_money;
import com.example.abysterpub.repository.PaymentMomoRepository;
import com.example.abysterpub.request.PaymentMomoRequest;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

@RestController
@RequestMapping("/payment")
public class PaymentController {

	
	@Autowired 
	PaymentMomoRepository paymentrep;
	
	@ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
	
	 @PostMapping(value = "/add")
	 public Paiement_mobile_money addPayment(@Valid @RequestBody PaymentMomoRequest paymentrequest) {
		 Paiement_mobile_money payment =new Paiement_mobile_money(); 
		 payment.setCle_marchand(paymentrequest.getCle_marchand());
		 payment.setNom_operateur(paymentrequest.getNom_operateur());
		 return paymentrep.save(payment);
	 }
}
