package com.example.abysterpub.controller;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import com.example.abysterpub.request.ChangePasswordRequest;
import com.example.abysterpub.request.LoginRequest;
import com.example.abysterpub.request.UserRequest;
import com.example.abysterpub.service.UserServiceImpl;
import com.example.abysterpub.utils.JwtTokenUtil;
import com.example.abysterpub.utils.OtpImpl;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import com.example.abysterpub.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.example.abysterpub.entities.Utilisateur;
import com.example.abysterpub.exceptions.AbysterpubFunctionalException;
import com.example.abysterpub.exceptions.EntitiesNotFoundException;
import com.example.abysterpub.exceptions.ErrorMessage;
import com.example.abysterpub.exceptions.RequestValidationException;
import java.time.LocalDateTime;


@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
	JwtTokenUtil tokenUtil;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
	private UserServiceImpl userService;
    
   
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name="Access-Control-Allow-Origin", value="*",paramType="header",required=true)
	})
	
    @ApiOperation(value="Add a User")
    @PostMapping(value = "/add")
    // responseEntity hérite de httpEntity et permet de définir le code à retourner
    
    public Utilisateur addUser(@Valid @RequestBody UserRequest userRequest) throws RequestValidationException{
    	Utilisateur utilisateur=new Utilisateur();
    	utilisateur.setId_utilisateur(utilisateur.getId_utilisateur());
    	utilisateur.setEmail(userRequest.getEmail());
    	utilisateur.setPassword(passwordEncoder.encode(userRequest.getPassword()));
    	utilisateur.setRole(userRequest.getRole());
    	utilisateur.setCreateDate(userRequest.getCreateDate());
    	utilisateur.setUpdateDate(userRequest.getUpdateDate());
    		
        Utilisateur userAdded =  userRepository.save(utilisateur);
       
        return userAdded;
        // réponse http pour dire que la requête POST s'est bien effectuée
        // avec un code 201 created
    }
    
    
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
    @GetMapping(value = "/findUserById/{id}")
    @ApiOperation(value="find a user by his Id")
    public Optional<Utilisateur> findUserById (@RequestParam int id) {
		return userRepository.findById(id);
    	
    }
    
    
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
    @GetMapping(value = "/findAllUsers")
    @ApiOperation(value="find all users")
    public void findAllUsers () {
		 userRepository.findAll();
    	
    }
   /*
    @PutMapping("/password-reset")
    @ApiOperation(value="reset password from a given token")
    public String resetPassword(@RequestParam String token,
			@RequestParam String password) {

		return userService.resetPassword(token, password);
	}
    */
    /*
    @PostMapping("/forgot-password")
    @ApiOperation(value="generate token to reset password")
	public String forgotPassword(@RequestParam String email) {

		String response = userService.forgotPassword(email);

		if (!response.startsWith("Invalid")) {
			response = "http://localhost:8080/password-reset?token=" + response;
		}
		return response;
	}
    
    @DeleteMapping("/deleteById")
    @ApiOperation("delete a user from his id")
    public void deleteUserById(int id) {
    	 userRepository.deleteById(id);
    }
    */
    
    
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
    @PutMapping("/updateUserRole")
    public Utilisateur updateUser(@Valid @RequestBody UserRequest userRequest,@PathVariable int id) {
    	Utilisateur utilisateur=userRepository.findById(id).orElse(null);
    	if(utilisateur!=null) {
    		utilisateur.setId_utilisateur(utilisateur.getId_utilisateur());
    		utilisateur.setRole(userRequest.getRole());
    		return utilisateur;
    	}
    	throw new EntitiesNotFoundException("le type de compte avec l'identifiant "+id+" est introuvable.");
    }
    
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
    @PostMapping("/login")
    @ApiOperation(value="permit to a user to login")
    public Object login(@Valid @RequestBody LoginRequest loginRequest) throws AbysterpubFunctionalException{
    	
    	Utilisateur utilisateur= userRepository.findByEmail(loginRequest.getEmail());
    	if(utilisateur!=null && (loginRequest.getEmail().equals(utilisateur.getEmail())) &&  (tokenUtil.isMatch(loginRequest.getPassword(),utilisateur.getPassword()))){ 
    		return utilisateur;
    	}else {
    	throw new AbysterpubFunctionalException("login ou mot de passe incorrect(s)");
    	}
    	}
   /* 
    @PostMapping(path = "/changepassword")
    public ResponseEntity<Object> changePassword(@RequestBody ChangePasswordRequest changePasswreq) {
        //logger.info("In Account Password update with parameter " + changePwdIn.toString());
        try {

            AccountOut out = accountService.changePwd(changePasswreq);
            return ResponseEntity.ok(out);

        } catch (OptivacFunctionnalException ofe) {
        	return ResponseEntity
					.status(HttpStatus.BAD_REQUEST)
					.body(new ErrorMessage(ofe.getMessage()));

        } catch (Exception ex) {
        	return ResponseEntity
			.status(HttpStatus.INTERNAL_SERVER_ERROR)
			.body(new ErrorMessage(ex.getMessage()));        }
    }
    */
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
    @PostMapping( "/otp/request/{email}")
    public  ResponseEntity<Object> requestOtpPassword(@RequestParam String email) throws AbysterpubFunctionalException {
    
    	try {
    	 Utilisateur out = userService.requestOtpPassword(email);
         return ResponseEntity.ok(out);
    	} catch( AbysterpubFunctionalException afe) {
    		return ResponseEntity
					.status(HttpStatus.BAD_REQUEST)
					.body(new ErrorMessage(afe.getMessage()));

    	}
    	catch (Exception ex) {
        	return ResponseEntity
        			.status(HttpStatus.INTERNAL_SERVER_ERROR)
        			.body(new ErrorMessage(ex.getMessage()));          
        }
    
    }
    
    @ApiImplicitParams({
        @ApiImplicitParam(name = "Authorization", value = "Bearer bcryptjwttoken", paramType = "header",required=true),
        @ApiImplicitParam(name = "Content-Type", value = "application/json", paramType = "header",required=true),
        @ApiImplicitParam(name = "Accept", value = "application/json", paramType = "header",required=true)
	})
    
    @PostMapping(path = "/changepassword",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "change a user password from an otp")
    public ResponseEntity<Object> changePassword(@RequestBody ChangePasswordRequest changePwd) {
        try {

            Utilisateur out = userService.changePwd(changePwd);
            return ResponseEntity.ok(out);

        } catch (AbysterpubFunctionalException afe) {
        	return ResponseEntity
					.status(HttpStatus.BAD_REQUEST)
					.body(new ErrorMessage(afe.getMessage()));

        } catch (Exception ex) {
        	return ResponseEntity
			.status(HttpStatus.INTERNAL_SERVER_ERROR)
			.body(new ErrorMessage(ex.getMessage()));        }
    }
    
}
   
    
