package com.example.abysterpub.response;


import java.io.Serializable;

/**
 *
 * 
 * This is class is required for creating a response containing the JWT to be returned to the userRequest
 *
 */
public class JwtResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	private final String jwttoken;
	
	/**
	 * constructor
	 * @param jwttoken
	 */
	public JwtResponse(String jwttoken) {
		super();
		this.jwttoken = jwttoken;
	}
	
	/**
	 * getter
	 * 
	 */
	
	public String getJwttoken() {
		return jwttoken;
	}
	

}
