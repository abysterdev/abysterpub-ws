-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 25 août 2020 à 23:31
-- Version du serveur :  10.1.40-MariaDB
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `abysterpub`
--

-- --------------------------------------------------------

--
-- Structure de la table `application`
--

CREATE TABLE `application` (
  `application_id` int(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `creation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `application`
--

INSERT INTO `application` (`application_id`, `name`, `token`, `active`, `creation`) VALUES
(1, 'ABYSTERPUB', 'Tdz65jUm1pZ8ZSwJNJOOU#', 1, '2020-08-23');

-- --------------------------------------------------------

--
-- Structure de la table `campagne`
--

CREATE TABLE `campagne` (
  `ID_CAMPAGNE` int(11) NOT NULL,
  `ID_UTILISATEUR` int(11) NOT NULL,
  `NOM` varchar(255) DEFAULT NULL,
  `OBJECTIF` varchar(255) DEFAULT NULL,
  `SPECIAL_AD_CATEGORIE` varchar(255) DEFAULT NULL,
  `STATUT_CAMPAGNE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table ADSET
(
   ADSET_ID             int not null,
   OPTIMIZATION_GOAL    varchar(1024) not null,
   BILLING_EVENT        varchar(1024) not null,
   BID_AMOUNT           int not null,
   BUDGET               int not null,
   TARGETING            varchar(1024) not null,
   START_TIME           date not null,
   END_TIME             date not null,
   NOM_ADSET            varchar(1024) not null,
   STATUT_ADSET         varchar(1024) not null,
   BOUTON_ACTION        varchar(1024) not null,
   primary key (ADSET_ID)
);

-- --------------------------------------------------------

--
-- Structure de la table `diffusion`
--

CREATE TABLE `diffusion` (
  `ID_DIFFUSION` int(11) NOT NULL,
  `ID_PUBLICITE` int(11) NOT NULL,
  `ID_UTILISATEUR` int(11) NOT NULL,
  `SUPPORT_DIFFUSION` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `paiement_mobile_money`
--

CREATE TABLE `paiement_mobile_money` (
  `ID_PAIEMENT` int(11) NOT NULL,
  `ID_UTILISATEUR` int(11) NOT NULL,
  `NOM_OPERATEUR` varchar(255) DEFAULT NULL,
  `CLE_MARCHAND` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `paiement_paypal`
--

CREATE TABLE `paiement_paypal` (
  `ID_PAIEMENT` int(11) NOT NULL,
  `ID_UTILISATEUR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `publicite`
--

CREATE TABLE `publicite` (
  `ID_PUBLICITE` int(11) NOT NULL,
  `FORMAT` varchar(255) DEFAULT NULL,
  `ACCROCHE` varchar(255) DEFAULT NULL,
  `TITRE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NOM_PUBLICITE` varchar(255) DEFAULT NULL,
  `STATUT_PUB` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `ID_UTILISATEUR` int(11) NOT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `NUMERO` int(10) DEFAULT NULL,
  `ROLE` varchar(255) DEFAULT NULL,
  `TOKEN` varchar(255) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`ID_UTILISATEUR`, `EMAIL`, `PASSWORD`, `NUMERO`, `ROLE`, `TOKEN`, `create_date`, `update_date`) VALUES
(1, 'kouamramses2@gmail.com', '$2a$10$3xLkm/dRblFT9nlgq8Seeu8cK9QVTOZ.w6L4j0IQIzAMTCRfgGJB2', NULL, 'admin', '416129', '2020-08-18 05:25:06', '2020-08-23 14:34:54'),
(2, 'string', '$2a$10$JLY0kYzbcWfY9vvvle6O7uC.YNwZwgeABfw4fBc0973uVslYlg.le', NULL, 'string', NULL, '2020-08-23 10:08:27', '2020-08-23 12:08:27');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`application_id`);

--
-- Index pour la table `campagne`
--
ALTER TABLE `campagne`
  ADD PRIMARY KEY (`ID_CAMPAGNE`),
  ADD KEY `FKt8lkf0v3l1umuhorrm2mddw6w` (`ID_UTILISATEUR`);

--
-- Index pour la table `diffusion`
--
ALTER TABLE `diffusion`
  ADD PRIMARY KEY (`ID_DIFFUSION`),
  ADD KEY `ID_PUBLICITE` (`ID_PUBLICITE`),
  ADD KEY `FKauxhud7xoi7v39xo4ptsy2j1p` (`ID_UTILISATEUR`);

--
-- Index pour la table `paiement_mobile_money`
--
ALTER TABLE `paiement_mobile_money`
  ADD PRIMARY KEY (`ID_PAIEMENT`),
  ADD KEY `FKn35k01ajp0wygltr9yn7dxfyk` (`ID_UTILISATEUR`);

--
-- Index pour la table `paiement_paypal`
--
ALTER TABLE `paiement_paypal`
  ADD PRIMARY KEY (`ID_PAIEMENT`),
  ADD KEY `FKbl8ds4ngw7794lv7fukwnnjbf` (`ID_UTILISATEUR`);

--
-- Index pour la table `publicite`
--
ALTER TABLE `publicite`
  ADD PRIMARY KEY (`ID_PUBLICITE`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`ID_UTILISATEUR`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `application`
--
ALTER TABLE `application`
  MODIFY `application_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `campagne`
--
ALTER TABLE `campagne`
  MODIFY `ID_CAMPAGNE` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `diffusion`
--
ALTER TABLE `diffusion`
  MODIFY `ID_DIFFUSION` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `paiement_mobile_money`
--
ALTER TABLE `paiement_mobile_money`
  MODIFY `ID_PAIEMENT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `paiement_paypal`
--
ALTER TABLE `paiement_paypal`
  MODIFY `ID_PAIEMENT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `publicite`
--
ALTER TABLE `publicite`
  MODIFY `ID_PUBLICITE` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `ID_UTILISATEUR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `campagne`
--
ALTER TABLE `campagne`
  ADD CONSTRAINT `FKt8lkf0v3l1umuhorrm2mddw6w` FOREIGN KEY (`ID_UTILISATEUR`) REFERENCES `utilisateur` (`ID_UTILISATEUR`),
  ADD CONSTRAINT `campagne_ibfk_1` FOREIGN KEY (`ID_UTILISATEUR`) REFERENCES `utilisateur` (`ID_UTILISATEUR`);

--
-- Contraintes pour la table `diffusion`
--
ALTER TABLE `diffusion`
  ADD CONSTRAINT `FKauxhud7xoi7v39xo4ptsy2j1p` FOREIGN KEY (`ID_UTILISATEUR`) REFERENCES `utilisateur` (`ID_UTILISATEUR`),
  ADD CONSTRAINT `diffusion_ibfk_1` FOREIGN KEY (`ID_UTILISATEUR`) REFERENCES `utilisateur` (`ID_UTILISATEUR`),
  ADD CONSTRAINT `diffusion_ibfk_2` FOREIGN KEY (`ID_PUBLICITE`) REFERENCES `publicite` (`ID_PUBLICITE`);

--
-- Contraintes pour la table `paiement_mobile_money`
--
ALTER TABLE `paiement_mobile_money`
  ADD CONSTRAINT `FKn35k01ajp0wygltr9yn7dxfyk` FOREIGN KEY (`ID_UTILISATEUR`) REFERENCES `utilisateur` (`ID_UTILISATEUR`),
  ADD CONSTRAINT `paiement_mobile_money_ibfk_1` FOREIGN KEY (`ID_UTILISATEUR`) REFERENCES `utilisateur` (`ID_UTILISATEUR`);

--
-- Contraintes pour la table `paiement_paypal`
--
ALTER TABLE `paiement_paypal`
  ADD CONSTRAINT `FKbl8ds4ngw7794lv7fukwnnjbf` FOREIGN KEY (`ID_UTILISATEUR`) REFERENCES `utilisateur` (`ID_UTILISATEUR`),
  ADD CONSTRAINT `paiement_paypal_ibfk_1` FOREIGN KEY (`ID_UTILISATEUR`) REFERENCES `utilisateur` (`ID_UTILISATEUR`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
